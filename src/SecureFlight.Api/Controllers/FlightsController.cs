﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IRepository<Flight> _flightRepository;
    private readonly IMapper _mapper;

    public FlightsController(
        IService<Flight> flightService, 
        IRepository<Flight> flightRepository,
        IMapper mapper)
        : base(mapper)
    {
        _flightService = flightService;
        _flightRepository = flightRepository;
        _mapper = mapper;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet("{originAirport}/{destinationAirport}")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetFlightsByOriginDestination(
        string originAirport, 
        string destinationAirport)
    {
        var flights = await _flightRepository.FilterAsync(f => f.OriginAirport == originAirport && f.DestinationAirport == destinationAirport);

        if (!flights.Any())
        {
            return new ErrorResponseActionResult
            {
                Result = new ErrorResponse
                {
                    Error = new Error
                    {
                        Code = ErrorCode.NotFound,
                        Message = $"No flights were found for the origin airport {originAirport} and destination airport {destinationAirport}"
                    }
                }
            };
        }

        return Ok(_mapper.Map<IReadOnlyList<FlightDataTransferObject>>(flights));
    }

}